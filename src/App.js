import Heading from './Components/Heading1';
import Employees from './Components/Employees';

const App = () => {
  return (
    <div>
      <Heading content="Manage employees"/>
      <Employees />
    </div>
  );
}

export default App;
