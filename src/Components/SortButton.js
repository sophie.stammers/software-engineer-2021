import React from "react";

const SortButton = ({ text, handleSort }) => (
  <button onClick={handleSort}> {text} </button>
);

export default SortButton;




