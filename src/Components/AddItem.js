import React from 'react';

import Heading from './Heading2'
import FormInput from './FormInput'

const AddItem = ({ firstName, lastName, email, onFNChange, onLNChange, onEmailChange, onAdd }) => (
  <div className="add-wrap">
    <Heading content="Add new" />
    <div className="form">
      <FormInput name="first-name" label="First Name*" type="text" value={firstName} handleChange={onFNChange} />
      <FormInput name="last-name" label="Last Name*" type="text" value={lastName} handleChange={onLNChange} />
      <FormInput name="email" label="Email" type="email" value={email} handleChange={onEmailChange} />
      <button className="add-button" type="button" onClick={onAdd}> Add </button>
    </div>
  </div>
);

export default AddItem;