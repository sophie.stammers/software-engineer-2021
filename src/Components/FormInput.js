import React from "react";

const FormInput = ({ name, label, type, value, handleChange }) => (
  <div className="form-input">
    <label htmlFor={name}> {label} </label>
    <input
      onChange={handleChange}
      value={value}
      name={name}
      type={type}
    />
  </div>
);

export default FormInput;