const Heading1 = ({ content }) => (
  <header className="heading1">
    <h1> {content} </h1>  
  </header>
);
export default Heading1;