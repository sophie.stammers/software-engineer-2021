const Heading2= ({ content }) => (
  <header className="heading2">
    <h2> {content} </h2>  
  </header>
);
export default Heading2;