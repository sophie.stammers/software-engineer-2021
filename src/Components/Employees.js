import React, {useState} from 'react';

import Heading from "./Heading2";
import AddItem from "./AddItem";
import SortButton from "./SortButton";

// Initial employees to populate page on load
const initialEmployees = [
  {
    firstName: "Jen",
    lastName: "Mitchell",
    email: "jm1@company.com"
  },
  {    
    firstName: "Sarah",
    lastName: "Beeston",
    email: "s.Beeston@company.com"
  },
  {    
    firstName: "Pete",
    lastName: "Raynes",
    email: "praynes11@company.com"
  },
  {    
    firstName: "Sajid",
    lastName: "Rasheed",
    email: "srsh44@company.com"
  },
  {    
    firstName: "Philip",
    lastName: "Kane",
    email: "pk11782@company.com"
  },
  {    
    firstName: "Ben",
    lastName: "Ross",
    email: "brr3342@company.com"
  },
  {    
    firstName: "Trayvon",
    lastName: "Lowe",
    email: "tl3342@company.com"
  },
  {    
    firstName: "Jess",
    lastName: "Lee",
    email: "jl32332@company.com"
  },
  {    
    firstName: "Daniel",
    lastName: "Black",
    email: "db4483@company.com"
  },
  {    
    firstName: "Claire",
    lastName: "Oliver",
    email: "co33882@company.com"
  },
];

const Employees = () => {
  const [employees, setEmployees] = useState(initialEmployees);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");

  function handleFirstName(event) { 
    // Set firstName in state as value from first-name FormInput field
    setFirstName(event.target.value);
  }

  function handleLastName(event) {
    // Set lastName in state as value from last-name FormInput field
    setLastName(event.target.value);
  }

  function handleEmail(event) {
    // Set email in state as value from email-name FormInput field
    setEmail(event.target.value);
  }

  function handleAdd() {
    // Trim whitespace from employee values 
    setFirstName(firstName.trim());
    setLastName(lastName.trim());
    setEmail(email.trim());

    // Ensure first and last name fields are filled before adding new employee
    if ((firstName === "") || (lastName === "")) {
      alert("Name fields cannot be blank");
    }
    
    // Create a new employee object, using values from form fields
    // Add new employee to updatedEmployees list
    const updatedEmployees = [...employees].concat({ firstName, lastName, email });

    // If names are non-empty, set employees list in state to be equal to the list we updated above
    if(firstName !== "" && lastName !== "") {
      setEmployees(updatedEmployees);
    }
    
    // Reset form fields 
    setFirstName("");
    setLastName("");
    setEmail("");    
  }

  // Call on a list objects to sort by their first name properties
  function compareFirstNames( a, b ) {
    if ( a.firstName.toLowerCase() < b.firstName.toLowerCase() ){
      return -1;
    }
    if ( a.firstName.toLowerCase() > b.firstName.toLowerCase() ){
      return 1;
    }
    return 0;
  }

  // Call on a list objects to sort by their last name properties
  function compareLastNames( a, b ) {
    if ( a.lastName.toLowerCase() < b.lastName.toLowerCase() ){
      return -1;
    }
    if ( a.lastName.toLowerCase() > b.lastName.toLowerCase() ){
      return 1;
    }
    return 0;
  }
  
  // Sort employees list by first names
  function handleSortFirstNames() {
    const updatedEmployees = [...employees].sort(compareFirstNames);
    setEmployees(updatedEmployees);
  }

  // Sort employees list by last names
  function handleSortLastNames() {
    const updatedEmployees = [...employees].sort(compareLastNames);
    setEmployees(updatedEmployees);
  }

  return (
    <div className="employees-wrap">

      <div className="current-wrap">
        <Heading content="Current" />        
        
        <div className="sort-btns">
          <SortButton text="Sort by first name" handleSort={handleSortFirstNames}/>      
          <SortButton text="Sort by last name" handleSort={handleSortLastNames}/>
        </div>

        <table className="table">
          <thead>
            <tr>
              <th>First name</th>
              <th>Last name</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
            {employees.map((item, index) => (
              <tr key={index}>
                <td>{item.firstName}</td> 
                <td>{item.lastName}</td> 
                <td>{item.email}</td>
              </tr>
            ))}
          </tbody> 
        </table>
      </div>    

      <AddItem
        firstName={firstName}
        lastName={lastName}
        email={email}
        onFNChange={handleFirstName}
        onLNChange={handleLastName}
        onEmailChange={handleEmail}
        onAdd={handleAdd}
      />      

    </div>
  );
};

export default Employees;

